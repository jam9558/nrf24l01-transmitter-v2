/*
This program is the first test of communication
utilizing the nRF24L01 RF Module.

By: Jack Milkovich
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "Common.h"
#include "SPI.h"
#include "uart.h"
#include "msp.h"
#include "NRF24L01.h"
#include <inttypes.h>

#define CHAR_COUNT 2

/*
	**CSN set low before every instruction
	**To write an instruction:
		- INSTRUCTION WORD <MSBit to LSBit (one byte)>
		- Data bytes <LSByte to MSByte, MSBit in each byte first>
		(ALL REGISTER INFO: PG. 22 - 26)
	**To read a register: address = 0b000XXXXX where X can be 1 or 0
	**To write a register: address = 0b001XXXXX where X can be 1 or 0
	
	steps for Receive setup:
	-(DONE)Use the same CRC config.
	-(DONE)Set PRIM_RX to 1
	-(DONE)Disable auto acknowledgement on data pipe that
	is addressed
	-(DONE)Use the same address width as PTX device
	-(DONE)Use the same frequency channel as PTX device
	-(DONE)Select data rate 1Mbit/s on both TX and RX devices
	-(DONE)Set correct payload width on data pipe that is addressed
	-(DONE)Set PWR_UP and CE High
	
	A byte = 0x01 = 8 bits
	
	
	**CSN set back to high at end of instruction
	*/
int main(void){
	uint8_t address;
	uint8_t RX_STATUS;
	uint64_t initDataLong = 0;
	int character_count = 0;
	char character[CHAR_COUNT+1];
	char temp[CHAR_COUNT+1];
	int result;
	
	//debug uart code
	uart0_init();
	
	//Init SPI stuff
	//Init_Recieve_NRF_SPI();

	Init_Transmit_NRF_SPI();

	//NRF_RX_Reg_Config();
	
	NRF_TX_Reg_Config();
	
	
	
	
	uart0_put("\r\nENTERING TX...\r\n");
	while(1){
		//Read user input to transmit first
		uart0_put("\r\nEnter a sentence:");
		if(character_count==0)
		{	
			// send carriage return/line feed to uart
			uart0_putchar('\r');
			uart0_putchar('\n');
		}
		while(character_count < CHAR_COUNT)
		{
			// read a character from the UART
			character[character_count] = uart0_getchar();

			
			// write the character to the UART
			uart0_putchar(character[character_count]);
		
			
			// if "RETURN" character, break
			if(character[character_count] == '\r'){
				break;
			}
			character_count++;
		}
		
		uart0_put("\r\nIn hex: ");
		result = atoi(character);
		sprintf(temp, "0x%x\r\n", result);
		uart0_put(temp);
		
		
		
		//TX CE low
		P4->OUT &= ~64;
		
		//WRITE receiver pipe address in TX_ADDR reg
		//set CSN to low
		P4->OUT &= ~2;
		//TX_ADDR
		address = 0x30;				
		initDataLong = 0x0101010101;	//5 byte addr width, addr = 0x01
		SPI_Transmit_transfer(address);
		SPI_Transmit_transfer(0x01);
		SPI_Transmit_transfer(0x01);
		SPI_Transmit_transfer(0x01);
		SPI_Transmit_transfer(0x01);
		SPI_Transmit_transfer(0x01);
		//set CSN to high
		P4->OUT |= 2;
		
	
		
		//W_TX_PAYLOAD command
		//set CSN to low
		P4->OUT &= ~2;
		//TX_PAYLOAD command
		address = 0xA0;				
		initDataLong = 0xBEEF;	//data to send
		SPI_Transmit_transfer(address);
		SPI_Transmit_transfer(result);
		SPI_Transmit_transfer(0xEF);
		//set CSN to high
		P4->OUT |= 2;
		
		//TX CE high
		P4->OUT |= 64;
		delayMs(10); //minimum of 10us
		//TX CE LOW
		P4->OUT &= ~64;
		
		//READ STATUS REG. for TX_DS and MAX_RT
		uart0_put("Reading... TX STATUS: ");
		read_Transmit_NRF_Register(0x07);
		
	
		
		
		
		clear_Interrupts();
		
		
		//Clear user input
		for(int i = 0; i < character_count; i++){
			character[i] = '\0';
			temp[i] = '\0';
		}
		character_count = 0;
		result = 0;
		
		delayMs(100);
		
	

		
		
	}
}
