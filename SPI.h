#include "msp.h"

void delayMs(int n);

void Init_Recieve_NRF_SPI(void);

void Init_Transmit_NRF_SPI(void);

void SPI_Recieve_transfer(uint8_t d);

uint16_t SPI_Recieve_recieve(void);

void SPI_Transmit_transfer(uint8_t d);

uint16_t SPI_Transmit_recieve(void);
